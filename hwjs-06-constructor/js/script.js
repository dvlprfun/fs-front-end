class Human {
  #name;
  #age;
  #hairstyle;

  constructor(name, age, hairStyle = "default") {
    this.#name = name;
    this.#age = age;
    this.#hairstyle = hairStyle;
  }

  getName() {
    return this.#name;
  }

  getAge() {
    return this.#age;
  }

  getHairstyle() {
    return this.#hairstyle;
  }

  setHairstyle(hairStyle) {
    this.#hairstyle = hairStyle;
  }
}

function sortByAge(array) {
  let arrayLength = array.length;

  // Buble sort
  for (let i = 0; i < arrayLength - 1; i++) {
    for (let j = i + 1; j < arrayLength; j++) {
      if (array[i].getAge() > array[j].getAge()) {
        [array[i], array[j]] = [array[j], array[i]];
      }
    }
  }
}

// Creating a test array of humans
const humans = [];

for (let i = 0; i < 10; i++) {
  humans.push(new Human(`Human0${i + 1}`, Math.round(Math.random() * 120) + 1));
}

// Show before sorting
for (let i = 0; i < humans.length; i++) {
  console.log(humans[i]);
}

sortByAge(humans);

console.log("-------------");
console.log("After sorting")

for (let i = 0; i < humans.length; i++) {
  console.log(humans[i]);
}