const styles = ["Джаз", "Блюз"];
let centerIndex;

console.log(styles);

styles.push("Рок-н-ролл");

console.log(styles);

// centerIndex = (styles.length - styles.length % 2) / 2;
centerIndex = Math.floor(styles.length / 2);
styles.splice(centerIndex, 1, "Классика");

console.log(styles);
console.log(styles.shift());
console.log(styles);

styles.unshift("Рэп", "Регги");

console.log(styles);