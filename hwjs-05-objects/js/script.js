const myDocument = {
  title: "",
  body: "",
  footer: "",
  date: "",
  annex: {
    title: "",
    body: "",
    footer: "",
    date: ""
  },
  showContent: function () {
    document.write(`<h3>${this.title}</h3>`);
    document.write(`<div>${this.body}</div>`);
    document.write(`<div>${this.footer}</div>`);
    document.write(`<div>${this.date}</div>`);
    document.write(`<h3>${this.annex.title}</h3>`);
    document.write(`<div>${this.annex.body}</div>`);
    document.write(`<div>${this.annex.footer}</div>`);
    document.write(`<div>${this.annex.date}</div>`);
  },
  addMainContent: function () {
    let title = prompt("Введите заголовок документа", "title");
    let body = prompt("Введите содержимое документа", "content");
    let footer = prompt("Добавьте нижний колонтитул", "footer");
    let date = new Date();

    this.title = title;
    this.body = body;
    this.footer = footer;
    this.date = date;
  },
  addAnnex: function () {
    let title = prompt("Введите заголовок приложения документа", "title");
    let body = prompt("Введите содержимое приложения документа", "content");
    let footer = prompt("Добавьте нижний колонтитул приложения документа", "footer");
    let date = new Date();

    this.annex.title = title;
    this.annex.body = body;
    this.annex.footer = footer;
    this.annex.date = date;
  }
};

// Show empty document
myDocument.showContent();
// Add the main content for the document
myDocument.addMainContent();
// Add the annex for the document
myDocument.addAnnex();
// Show document after content has been added
myDocument.showContent();