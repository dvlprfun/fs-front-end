// Создайте массив размерностью 15 элементов, выведите все элементы в обратном порядке и разделите каждый элемент спецсимволом "Облака". При загрузке страницы спросите у пользователя индекс и удалите этот элемент из массива.

const arr = [];

let arrSize = arr.length = 15;

for (let i = 0; i < arrSize; i++) {
  arr[i] = i;
}

arr.reverse();

let str = arr.join("&#9729;");

document.write(str + "<br>");

let index = +prompt("Введите удаляемый элемент");

if (isNaN(index) || index > arrSize - 1 || index < 0) {
  document.write("Ошибка: введено недопустимое значение");
} else {
  arr.splice(index, 1);
  document.write(arr);
}