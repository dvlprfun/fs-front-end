const DATA_TYPE = 'string';

function filterBy(array, dataType) {
  return array.filter(item => typeof item !== dataType);
}

const arr = ['11', 22, 'Hello', null, 41, '34'];

// Show array before sorting
console.log(arr);

// Show array after sorting
console.log(`-- After sorting all elements of type '${DATA_TYPE}' will be deleted. --`);
console.log(filterBy(arr, DATA_TYPE));