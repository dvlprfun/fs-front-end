// Start of code added as homework
function calculator(a = 0, b = 0, callback) {
  let validateInfo = isDataCorrect(arguments);

  return validateInfo === 0 ? callback(a, b) : validateInfo;
}
// End of code added as homework

const add = (a, b) => {
  return a + b;
}

const sub = (a, b) => {
  return a - b;
}

const mult = (a, b) => {
  return a * b;
}

const div = (a, b) => {
  return a / b;
}

function getData() {
  let operand1 = Number.parseInt(prompt("Input first argument:"));
  let operand2 = Number.parseInt(prompt("Input second argument:"));
  let sign = prompt("Input operation");

  switch (sign) {
    case '+': return calculator(operand1, operand2, add);
    case '-': return calculator(operand1, operand2, sub);
    case '*': return calculator(operand1, operand2, mult);
    case '/': return calculator(operand1, operand2, div);
  }
}

function show() {
  document.write(getData());
};

// Start of code added as homework
function isDataCorrect (argsArr) {
  let a = argsArr[0];
  let b = argsArr[1];
  let operator = argsArr[2];

  if (isNaN(a) || isNaN(b)) {
    return "Non-numeric value entered";
  } else if (operator === div && b === 0) {
    return "Division by zero is not allowed";
  } else if (argsArr.length !== 3) {
    return "Invalid number of arguments";
  }

  return 0;
}
// End of code added as homework

show();