function getById(id) {
  return document.getElementById(id);
}

class Timer {
  currentTime = {
    minutes: 0,
    seconds: 0,
    milliseconds: 0
  }

  getMinutes = () => {
    return this.currentTime.minutes;
  }

  getSeconds = () => {
    return this.currentTime.seconds;
  }

  getMilliseconds = () => {
    return this.currentTime.milliseconds;
  }

  isPaused = false;
  isStarted = false;

  start = () => {
    if (!this.isStarted) {
      setInterval(() => {
        if (!this.isPaused) {
          this.increaseTime();
        }
      }, 10);
      this.isStarted = true;
    } else {
      this.isPaused = false;
    }
  }

  stop = () => {
    this.isPaused = true;
  }

  increaseTime = () => {
    if (this.currentTime.milliseconds < 99) {
      this.currentTime.milliseconds++;
    } else if (this.currentTime.seconds < 59) {
      this.currentTime.milliseconds = 0;
      this.currentTime.seconds++;
    } else if (this.currentTime.minutes < 99) {
      this.currentTime.milliseconds = 0;
      this.currentTime.seconds = 0;
      this.currentTime.minutes++;
    } else {
      this.currentTime.milliseconds = 0;
      this.currentTime.seconds = 0;
      this.currentTime.minutes = 0;
    }
  }
}

const startTimer = getById("start"),
  stopTimer = getById("stop"),
  resetTimer = getById("reset"),
  stopwatchScreen = document.querySelector(".stopwatch__screen"),
  minutes = getById("minutes"),
  seconds = getById("seconds"),
  milliseconds = getById("milliseconds");

let timer = new Timer();

startTimer.onclick = () => {
  timer.start();
  stopwatchScreen.setAttribute("style", "background-color: #0B3006");
};

stopTimer.onclick = () => {
  timer.stop();
  stopwatchScreen.setAttribute("style", "background-color: #910303");
};

resetTimer.onclick = () => {
  timer = new Timer();
  stopwatchScreen.setAttribute("style", "background-color: grey");
};

function getComputedDigitSection(timeNumber) {
  return timeNumber < 0 ? -1 : timeNumber < 10 ? `0${timeNumber}` : timeNumber;
}

function showTime() {
  setInterval(() => {
    minutes.innerText = getComputedDigitSection(timer.getMinutes());
    seconds.innerText = getComputedDigitSection(timer.getSeconds());
    milliseconds.innerText = getComputedDigitSection(timer.getMilliseconds());
  }, 1);
}

showTime();