const sliderContainer = document.querySelector('.slider-container');
const sliderImg = document.createElement('img');

sliderImg.setAttribute('style', 'display: block; width: 100%; height: 100%');

sliderContainer.append(sliderImg);

imgList = [
  "https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
  "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
  "https://cdnimg.rg.ru/img/content/180/74/91/1923_d_850_t_650x433.jpg",
  "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
  "https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg"
];

class Slider {
  #imgList;
  #sliderImg;
  #currentImgIndex = 0;

  constructor(imgList, sliderImg) {
    this.#imgList = imgList;
    this.#sliderImg = sliderImg;
    this.#sliderImg.src = this.#imgList[this.#currentImgIndex % this.#imgList.length];
    this.#currentImgIndex++;
  }

  start = () => {
    setInterval(() => {
      this.#sliderImg.src = this.#imgList[this.#currentImgIndex % this.#imgList.length];
      this.#currentImgIndex++;
    }, 3000);
  }
}

const slider = new Slider(imgList, sliderImg);

slider.start();