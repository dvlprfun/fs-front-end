divCreate = () => document.createElement('div');

const wrapper = document.querySelector(".wrapper");

const phoneChecker = divCreate();
const phoneInput = document.createElement('input');
const phoneBtn = divCreate();
const phoneBtnText = document.createElement('span');
const phoneInfo = divCreate();

phoneChecker.classList.add("phone-checker");
phoneInput.classList.add("phone-checker__input");
phoneBtn.classList.add('phone-checker__btn');

phoneInput.placeholder = "000-000-00-00";
phoneInput.title = "Input phone number";
phoneBtnText.innerText = "Save";
phoneInfo.innerText = "Success";

wrapper.append(phoneChecker);
phoneChecker.append(phoneInfo);
phoneChecker.append(phoneInput);
phoneChecker.append(phoneBtn);
phoneBtn.append(phoneBtnText);

phoneInfo.setAttribute('style', 'visibility: hidden');

phoneBtn.onclick = () => {
  validatePhoneNumber(phoneInput.value);
}

function validatePhoneNumber(phoneNumber) {
  let phoneRegexp = /^(\d{3}-){2}\d{2}-\d{2}$/;

  phoneInfo.setAttribute('style', 'visibility: show');

  if (phoneRegexp.test(phoneNumber)) {
    phoneInfo.innerText = "Success";
    setTimeout(() => document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg", 1500);
    phoneInfo.classList.remove('phone-checker__info_fail');
    phoneInfo.classList.add('phone-checker__info_success');
    phoneInput.setAttribute('style', 'background-color: green');
  } else {
    phoneInfo.innerText = "Fail";
    phoneInfo.classList.remove('phone-checker__info_success');
    phoneInfo.classList.add('phone-checker__info_fail');
  }
}