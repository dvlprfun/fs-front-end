// Expression 1
var x = 6;
var y = 14;
var z = 4;

document.write("Expression 1 = " + (x += y - x++ * z) + "<br>");
// Результат = -4;
// 1. x * z = 24;
// 2. x++ = 7;
// 3. у - 24 = -10;
// 4. 6 + (-10) = -4;
// 5. x = -4
// ---


// Expression 2
var x = 6;
var y = 14;
var z = 4;

document.write("Expression 2 = " + (z = --x - y * 5) + "<br>");
// Результат = -65;
// 1. y * 5 = 70;
// 2. x - 1 = 5;
// 3. 5 - 70 = -65;
// z = -65;
// ---


// Expression 3
var x = 6;
var y = 14;
var z = 4;

document.write("Expression 3 = " + (y /= x + 5 % z) + "<br>");
// Результат = 2;
// 1. 5 % z = 1;
// 2. x + 1 = 7;
// 3. y / 7 = 2;
// 4. y = 2;
// ---


// Expression 4
var x = 6;
var y = 14;
var z = 4;

document.write("Expression 4 = " + (z - x++ + y * 5) + "<br>");
// Результат = 68;
// 1. y * 5 = 70;
// 2. z - x = -2;
// 3. x + 1 = 2;
// 4. -2 + 70 = 68;
// ---


// Expression 5
var x = 6;
var y = 14;
var z = 4;

document.write("Expression 5 = " + (x = y - x++ * z) + "<br>");
// Результат = -10;
// 1. x * z = 24;
// 2. y - 24 = -10;
// 3. x = -10;
// ---