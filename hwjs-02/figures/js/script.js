// Прямоугольник
let size = 10;

for (let i = 0; i < size; i++) {
  for (let j = 0; j < size; j++) {
    document.write("*&nbsp");
  }

  document.write("<br>");
}
// Отступ
for (let i = 0; i < 3; i++) {
  document.write("<br>");
}

// Прямоугольный треугольник
for (let i = 0; i < size; i++) {
  for (let j = size - (i + 1); j < size; j++) {
    document.write("*&nbsp");
  }

  document.write("<br>");
}
// Отступ
for (let i = 0; i < 3; i++) {
  document.write("<br>");
}

// Равносторонний треугольник
for (let i = 0; i < size; i++) {

  const LINE = i * 2 + 1;
  
  for (let j = LINE; j < size * 2; j++) {
    document.write("&nbsp");
  }
  
  for (let z = LINE; z > 0; z--) {
    document.write("*");
  }

  document.write("<br>");
}
// Отступ
for (let i = 0; i < 3; i++) {
  document.write("<br>");
}

// Ромб
for (let i = 0; i < size - 1; i++) {

  const LINE = i * 2 + 1;
  
  for (let j = LINE; j < size * 2; j++) {
    document.write("&nbsp");
  }
  
  for (let z = LINE; z > 0; z--) {
    document.write("*");
  }

  document.write("<br>");
}

for (let i = 0; i < size; i++) {
  const LINE = i * 2 + 1;

  for (let j = LINE; j > 0; j--) {
    document.write("&nbsp");
  }

  for (let z = 0; z < size * 2 - LINE; z++) {
    document.write("*");
  }

  document.write("<br>");
}