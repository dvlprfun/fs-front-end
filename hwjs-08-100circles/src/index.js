// Get the main button on the page
const btnCircles = document.querySelector('.paint-circles-btn');
// Get the wrapper of the page
const wrapper = document.querySelector('.wrapper');

// Set event for the main button of the page
btnCircles.onclick = function () {
  const btnPaint = document.createElement('button');
  const textInput = document.createElement('input');
  const container = document.createElement('div');

  container.classList.add('circles-container');
  wrapper.append(container);
  textInput.type = 'number';
  textInput.placeholder = 'Введите диаметр круга';
  btnPaint.innerHTML = "Нарисовать";
  btnPaint.classList.add('paint-circles-btn');
  container.append(textInput);
  container.append(btnPaint);
  this.disabled = true;

  btnPaint.onclick = function () {
    this.disabled = true;

    const circlesContainer = document.createElement('div');
    const circlesColor = [];

    circlesContainer.classList.add('circles-wrapper');
    circlesContainer.setAttribute('style', `min-width: ${textInput.value * 10}%`);

    for (let i = 0; i < 100; i++) {
      circlesColor.push(new Circle(Circle.getRandomColor(), textInput.value));
    }

    const btnReset = document.createElement('button');

    container.append(circlesContainer);
    container.append(btnReset);
    btnReset.innerHTML = "Сброс"
    btnReset.classList.add('paint-circles-btn');

    btnReset.onclick = function () {
      container.remove();
      btnCircles.disabled = false;
    }

    for (let i = 0; i < 100; i++) {
      let circleItem = circlesContainer.appendChild(document.createElement('div'));
      let tempElement = circleItem.appendChild(document.createElement('div'));

      circleItem.classList.add('circle-item');
      circleItem.setAttribute('style', `min-width: ${textInput.value}px`);
      tempElement.classList.add('simple-circle');
      tempElement.setAttribute('style', `min-width: ${circlesColor[i].diameter}px; min-height: ${circlesColor[i].diameter}px; background-color: ${circlesColor[i].color}`);

      // Add remove event for circle onclick
      tempElement.onclick = function () {
        circleItem.remove();
      }
    }
  }
}

class Circle {
  color;
  diameter;

  constructor(color = "green", diameter) {
    this.color = color;
    this.diameter = diameter === '' ? 25 : diameter;
  }

  static getRandomColor() {
    let red = Math.floor(Math.random() * 240);
    let green = Math.floor(Math.random() * 240);
    let blue = Math.floor(Math.random() * 240);

    return `#${red.toString(16)}${green.toString(16)}${blue.toString(16)}`;
  }
}
