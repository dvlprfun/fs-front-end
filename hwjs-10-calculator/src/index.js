/* * В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
* При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
* При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
* Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
* При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.
 */

window.addEventListener("DOMContentLoaded", () => {
    class Calculator {
        constructor(buttons, display) {
            this.buttons = buttons;
            this.display = display;
        }
    }


    const btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input");

    btn.addEventListener("click", function (e) {
        switch (e.target.value) {
            case '+':
                calc.sign = '+';
                break;
            case '-':
                calc.sign = '-';
                break;
            case '*':
                calc.sign = '*';
                break;
            case '/':
                calc.sign = '/';
                break;
            case '=':
                calculate(calc);
                break;
            case 'C':
                clearCalculator(calc);
                break;
            case 'm+':
                calc.inMemory += result;
                break;
            case 'm-':
                calc.inMemory -= result;
                break;
            case 'mrc':
                calc.inMemory = 0;
                break;
        }

        if (calc.sign === "") {
            calc.value1 += e.target.value;
            show(calc.value1, display);
        } else {
            // calc.sign = '';
            calc.value2 += e.target.value;
            show(calc.value2, display);
        }

        const forScreen = calc.value

    });

    calc = {
        value1: "",
        value2: "",
        sign: "",
        result: 0,
        inMemory: 0
    }

    function clearCalculator(calc) {
       calc.value1 = "";
       calc.value2 = "";
       calc.sign = "";
       calc.result = 0; 
    }

    function show(value, el) {
        el.value = value;
    }
});