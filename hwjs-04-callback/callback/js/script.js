function map(fn, array) {
  const newArr = [];

  for (let i = 0; i < array.length; i++) {
    newArr[i] = fn(array[i]);
  }

  return newArr;
}

function square(a) {
  return a ** 2;
}

function increment(a) {
  return ++a;
}

const array = [1, 2, 3, 4, 5, 6, 7];

document.write(`Исходный массив: ${array} <br>`);
document.write(`Возведение всех элементов массива в квадрат: ${map(square, array)} <br>`);
document.write(`Увеличение значений всех элементов массива на единицу: ${map(increment, array)}`);