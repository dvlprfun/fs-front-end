// function checkAge() {
//   if (age > 18) {
//     return true;
//   } else {
//     return confirm('Родители разрешили?');
//   }
// }

let age = +prompt('Сообщите свой возраст');

if (isNaN(age) || age < 1 || age > 123) {
  document.write('Введены некорретные данные');
} else {
  document.write(checkAge(age));

  // Второй вариант проверки возраста
  // document.write(age > 18 || confirm('Родители разрешили?'));
}

function checkAge(age) {
  return age > 18 ? true : confirm('Родители разрешили?');
}